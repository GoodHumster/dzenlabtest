//
//  ApiServices.m
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiServicesImpl.h"
#import <SocketRocket/SocketRocket.h>

NSString *const HOST = @"ws://52.29.182.220:8080/customer-gateway/customer";

NSString *const LOGIN = @"LOGIN_CUSTOMER";
NSString *const LOGIN_SUCCESS = @"CUSTOMER_API_TOKEN";
NSString *const LOGIN_FAILURE = @"CUSTOMER_ERROR";

@interface ApiServicesImpl ()<SRWebSocketDelegate>

@property (nonatomic, strong) SRWebSocket *webSocket;

@property (nonatomic, strong) id<RACSubscriber> currentSubscriber;


@end

@implementation ApiServicesImpl

-(instancetype) init
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    
    NSURL *url = [NSURL URLWithString:HOST];
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url];
    
    self.webSocket = [[SRWebSocket alloc] initWithURLRequest:urlRequest];
    self.webSocket.delegate = self;
    [self.webSocket open];
    
    return self;
}

-(RACSignal *) loginWithUserName:(NSString *)user password:(NSString *)password
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
       
        self.currentSubscriber = subscriber;
        NSDictionary *dict = @{@"type":LOGIN,
                               @"sequence_id":@"09caaa73-b2b1-187e-2b24-683550a49b23",
                               @"data":@{@"email":user,
                                         @"password":password}
                               };
        NSString *data = [NSString stringWithFormat:@"{\"type\":\"%@\", \"sequence_id\":\"09caaa73-b2b1-187e-2b24-683550a49b23\", \"data\":{ \"email\":\"%@\", \"password\":\"%@\"}}",LOGIN,user,password];
        NSLog(@"%@",data);
        [self.webSocket send:data];
        
        return nil;
    }];
}

-(void) webSocketDidOpen:(SRWebSocket *)webSocket
{
    NSLog(@"%s","[DEBUG] Web sockets opened");
}

-(void) webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
    NSLog(@"%@",message);
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[message dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    if (dict == nil)
    {
        NSError *error = [NSError errorWithDomain:@"wss" code:0 userInfo:@{@"description":@"Recive message empty"}];
        [self.currentSubscriber sendError:error];
    }
    
    if ([dict[@"type"] isEqualToString:LOGIN_SUCCESS])
    {
        [self.currentSubscriber sendNext:dict[@"data"]];
        [self.currentSubscriber sendCompleted];
    }
    
    if ([dict[@"type"] isEqualToString:LOGIN_FAILURE])
    {
        NSError *error = [NSError errorWithDomain:@"wss" code:0 userInfo:@{@"description":dict[@"data"][@"error_description"]}];
        [self.currentSubscriber sendError:error];
    }
}

-(void) webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    [self.currentSubscriber sendError:error];
    NSLog(@"%@",error);
}

@end