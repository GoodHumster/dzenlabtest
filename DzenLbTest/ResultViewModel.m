//
//  ResultViewModel.m
//  DzenLbTest
//
//  Created by ios on 24.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResultViewModel.h"
#import "ViewModelServices.h"

@interface ResultViewModel()

@property (nonatomic, weak) id<ViewModelServices> services;

@property (nonatomic, strong) NSString *apiKey;
@property (nonatomic, strong) NSDate *date;

@end

@implementation ResultViewModel

-(instancetype) initWithServices:(id)services dictionary:(NSDictionary *)dict
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.services = services;
    self.apiKey = dict[@"api_token"];
    NSDateFormatter *dateFormmater = [[NSDateFormatter alloc] init];
    dateFormmater.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    self.date = [dateFormmater dateFromString:dict[@"api_token_expiration_date"]];
    return self;
}

-(NSString *)getApiKey
{
    return self.apiKey;
}

-(NSString *)getDate
{
    NSDateFormatter *dateFormmater = [[NSDateFormatter alloc] init];
    dateFormmater.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [dateFormmater stringFromDate:self.date];
}

@end