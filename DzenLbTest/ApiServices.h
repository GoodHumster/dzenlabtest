//
//  ApiServices.h
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@protocol ApiServices <NSObject>

-(RACSignal *) loginWithUserName:(NSString *)user password:(NSString *)password;


@end