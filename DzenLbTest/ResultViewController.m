//
//  ResultViewController.m
//  DzenLbTest
//
//  Created by ios on 24.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResultViewController.h"
#import "ResultViewModel.h"
#import "SoulRegistratedResources.h"

@interface ResultViewController ()

@property (nonatomic, strong) ResultViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation ResultViewController

+(void) load
{
    [SoulRegistratedResources registeratedClass:[ResultViewController class]];
}

-(instancetype) initWithViewModel:(ResultViewModel *)viewModel
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    return self;
}


-(void) viewDidLoad
{
    [super viewDidLoad];
    self.dateLabel.text = [self.viewModel getDate];
    
}

#pragma mark - CoreBaseUI methods

+(Class) soulViewModelClass
{
    return [ResultViewModel class];
}

+(instancetype) initWithViewModel:(id)viewModel
{
    return [[ResultViewController alloc] initWithViewModel:viewModel];
}

@end