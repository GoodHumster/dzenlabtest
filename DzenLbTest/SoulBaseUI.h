//
//  SoulBaseUI.h
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol SoulBaseUI <NSObject>

/* return class registered view model
 */
+(Class) soulViewModelClass;

/* intialize resource with view model
 */
+(id<SoulBaseUI>) initWithViewModel:(id)viewModel;


@end