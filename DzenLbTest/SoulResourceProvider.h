//
//  SoulResourceProvider.h
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoulResourceProvider : NSObject

+(SoulResourceProvider *)sharedInstance;

-(id) provideResourceForViewModel:(id)viewModel;

@end