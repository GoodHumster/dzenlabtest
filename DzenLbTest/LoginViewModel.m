//
//  LoginViewModel.m
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginViewModel.h"
#import "ViewModelServices.h"
#import "ResultViewModel.h"

NSString *const kLoginApiKey = @"apiKey";

@interface LoginViewModel()

@property (nonatomic, weak) id<ViewModelServices> services;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *errorString;

@end

@implementation LoginViewModel

#pragma mark - instance methods

-(instancetype) initWithServices:(id)serivces
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.services = serivces;
    [self initialize];
    return self;
}

-(void) initialize
{
    self.errorSignal = [RACObserve(self, errorString) skip:1];
    RACSignal *enableSignal = [[RACSignal combineLatest:@[RACObserve(self, email),RACObserve(self, password)] reduce:^id(NSString *email,NSString *password){
        
        NSError *error;
        NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:@"^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$" options:0 error:&error];
        
        NSUInteger result = 0;
        if (email != nil || email.length != 0)
        {
          result  = [regularExpression numberOfMatchesInString:email options:0 range:NSMakeRange(0, email.length)];
        }
        
        BOOL validited = result != 0;
        
        return [NSNumber numberWithBool:self.password.length > 4 && validited];
    }] map:^id(id value) {
        return @([value boolValue]);
    }];
    
    
    __weak typeof(self) weakSelf = self;
    self.loginCommand = [[RACCommand alloc]initWithEnabled:enableSignal signalBlock:^RACSignal *(id input) {
       return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
           [[[self.services getApiServices] loginWithUserName:self.email password:self.password] subscribeNext:^(id value) {
               __strong typeof (weakSelf) blockSelf = weakSelf;
             NSData *data = [NSKeyedArchiver archivedDataWithRootObject:value];
             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
             [userDefaults setObject:data forKey:kLoginApiKey];
             ResultViewModel *viewModel = [[ResultViewModel alloc] initWithServices:self.services dictionary:value];
             [blockSelf.services pushViewModel:viewModel animated:YES];
               
           } error:^(NSError *error) {
               __strong typeof (weakSelf) blockSelf = weakSelf;
               blockSelf.errorString = error.userInfo[@"description"];
               NSLog(@"%@",error);
           }];
           [subscriber sendCompleted];
           return nil;
       }];
    }];
}

#pragma mark - Publick API methods

-(void) enterEmail:(NSString *)email
{
    self.email = email;
}

-(void) enterPassword:(NSString *)password
{
    self.password = password;
}

@end