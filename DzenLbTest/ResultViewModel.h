//
//  ResultViewModel.h
//  DzenLbTest
//
//  Created by ios on 24.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResultViewModel : NSObject

-(instancetype) initWithServices:(id)services dictionary:(NSDictionary *)dict;

-(NSString *)getApiKey;
-(NSString *)getDate;

@end