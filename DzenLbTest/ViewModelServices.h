//
//  ViewModelServices.h
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiServices.h"


@protocol ViewModelServices <NSObject>


-(void) pushViewModel:(id)viewModel animated:(BOOL) animated;
-(void) popAnimated:(BOOL)animated;
-(id<ApiServices>) getApiServices;


@end
