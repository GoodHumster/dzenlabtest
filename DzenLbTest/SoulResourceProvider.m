//
//  SoulResourceProvider.m
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "SoulResourceProvider.h"
#import "SoulRegistratedResources.h"
#import "SoulBaseUI.h"


@implementation SoulResourceProvider

+(SoulResourceProvider *)sharedInstance
{
    static SoulResourceProvider* _sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[SoulResourceProvider alloc] init];
    });
    
    return _sharedInstance;
}


-(id) provideResourceForViewModel:(id)viewModel
{
    NSArray *classes = [SoulRegistratedResources classArray];
    
    for  (Class<SoulBaseUI> class in classes)
    {
        Class viewModelClass = [class soulViewModelClass];
        if ([viewModel isKindOfClass:viewModelClass])
        {
            return [class initWithViewModel:viewModel];
        }
    }
    
    return nil;
}


@end