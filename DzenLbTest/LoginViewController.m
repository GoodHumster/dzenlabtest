//
//  LoginViewController.m
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginViewController.h"
#import "LoginViewModel.h"
#import "SoulRegistratedResources.h"


@interface LoginViewController()

@property (nonatomic, strong) LoginViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LoginViewController

+(void) load
{
    [SoulRegistratedResources registeratedClass:[LoginViewController class]];
}

-(instancetype) initWithViewModel:(LoginViewModel *)viewModel
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    return self;
}

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    UIView *emailPaddingLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 18)];
    self.email.leftView = emailPaddingLeft;
    self.email.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *passwordPaddingLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 18)];
    self.password.leftView = passwordPaddingLeft;
    self.password.leftViewMode = UITextFieldViewModeAlways;
    
    self.loginButton.layer.borderColor = [UIColor colorWithRed:76.0/255.0 green:175.0/255.0 blue:80.0/255.0 alpha:1.0].CGColor;
    self.loginButton.layer.borderWidth = 1;
    self.loginButton.layer.cornerRadius = 4;
    
    [self bindViewModel];
    
}

-(void) bindViewModel
{
    self.loginButton.rac_command = self.viewModel.loginCommand;
    
    __weak typeof(self) weakSelf = self;
    [self.loginButton.rac_command.enabled subscribeNext:^(id value) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if ([value boolValue])
        {
          blockSelf.loginButton.backgroundColor =  [UIColor colorWithRed:76.0/255.0 green:175.0/255.0 blue:80.0/255.0 alpha:1.0];
        } else {
          blockSelf.loginButton.backgroundColor = [UIColor colorWithRed:59.0/255.0 green:64.0/255.0 blue:84.0/255.0 alpha:1.0];

        }
    }];
    
    [self.viewModel.errorSignal subscribeNext:^(id value) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:value delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
        [alertView show];
    }];
    
   [self.email.rac_textSignal subscribeNext:^(id value) {
       __strong typeof(weakSelf) blockSelf = weakSelf;
        [blockSelf.viewModel enterEmail:value];
    }];
    
    [self.password.rac_textSignal subscribeNext:^(id value) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        [blockSelf.viewModel enterPassword:value];
    }];
    
}

#pragma mark - CoreBaseUI methods

+(Class) soulViewModelClass
{
    return [LoginViewModel class];
}

+(instancetype) initWithViewModel:(id)viewModel
{
    return [[LoginViewController alloc] initWithViewModel:viewModel];
}
 

@end