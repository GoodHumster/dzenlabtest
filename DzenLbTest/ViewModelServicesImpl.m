//
//  ViewModelServicesImpl.m
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModelServicesImpl.h"
#import "SoulResourceProvider.h"
#import "LoginViewModel.h"
#import "ApiServicesImpl.h"
#import "ResultViewModel.h"




@interface ViewModelServicesImpl ()


@property (nonatomic, retain) UINavigationController *navigationController;
@property (nonatomic, strong) ApiServicesImpl *apiServices;

@end


@implementation ViewModelServicesImpl

-(instancetype) initWithNavigationController:(UINavigationController *)navigationController
{
    if ((self = [super init]) == nil)
    {
        return nil;
    }
    self.navigationController = navigationController;
    [self initialized];
    [self switchToAutrizatedIfNeeded];
    return self;
}

-(void) initialized
{
    self.apiServices = [[ApiServicesImpl alloc] init];
}

#pragma mark - Publick API methods

-(void) pushViewModel:(id)viewModel animated:(BOOL)animated
{
    SoulResourceProvider *resourceProvider = [SoulResourceProvider sharedInstance];
    UIViewController *vc = [resourceProvider provideResourceForViewModel:viewModel];
    
    [self pushViewController:vc animated:animated];
}

-(void) popAnimated:(BOOL)animated
{
    [self.navigationController popViewControllerAnimated:animated];
}

-(id) getApiServices
{
    return self.apiServices;
}

#pragma mark - utilites methods

-(void) pushViewController:(UIViewController *)vc animated:(BOOL)animated
{
    self.navigationController.delegate = nil;
    [self.navigationController pushViewController:vc animated:animated];
}

-(void) switchToAutrizatedIfNeeded
{
    NSUserDefaults *userDefults =[NSUserDefaults standardUserDefaults];
    
    __weak typeof(self) weakSelf = self;
    void(^displayLogin)(void) = ^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        LoginViewModel *viewModel = [[LoginViewModel alloc] initWithServices:self];
        [blockSelf pushViewModel:viewModel animated:YES];
    };
    
    if (![userDefults objectForKey:kLoginApiKey])
    {
        displayLogin();
        return;
    }
    
    NSData *data = [userDefults objectForKey:kLoginApiKey];
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (dict == nil)
    {
        displayLogin();
        return;
    }
    
    NSDateFormatter *dateFormmater = [[NSDateFormatter alloc] init];
    dateFormmater.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *date = [dateFormmater dateFromString:dict[@"api_token_expiration_date"]];
    
    if ([date timeIntervalSinceNow] < 0)
    {
        displayLogin();
    }
    
    ResultViewModel *viewModel = [[ResultViewModel alloc] initWithServices:self dictionary:dict];
    [self pushViewModel:viewModel animated:YES];
    
    
    
}

@end