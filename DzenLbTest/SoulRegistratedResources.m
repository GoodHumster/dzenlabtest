//
//  SoulRegistratedResources.m
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "SoulRegistratedResources.h"
#import <objc/runtime.h>

@interface SoulRegistratedResources ()

@property (nonatomic, strong) NSArray *classArray;

@end


@implementation SoulRegistratedResources

+(void) registeratedClass:(Class)registratedClass
{
    @synchronized(self)
    {
        
        NSMutableArray *mutable = [[NSMutableArray alloc] initWithArray:objc_getAssociatedObject(self, @selector(classArray))];
        if (mutable == nil)
        {
            mutable = [[NSMutableArray alloc] init];
        }
        [mutable addObject:registratedClass];
        objc_setAssociatedObject(self, @selector(classArray), mutable, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

+ (NSArray *)classArray
{
    @synchronized(self) {
        return  objc_getAssociatedObject(self, @selector(classArray));
    }
}


@end