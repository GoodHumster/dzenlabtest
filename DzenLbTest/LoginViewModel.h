//
//  LoginViewModel.h
//  DzenLbTest
//
//  Created by ios on 23.05.16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface LoginViewModel : NSObject

-(instancetype) initWithServices:(id)serivces;

@property (nonatomic, strong) RACCommand *loginCommand;
@property (nonatomic, strong) RACSignal *errorSignal;

-(void) enterEmail:(NSString *)email;

-(void) enterPassword:(NSString *)password;

@end


extern NSString *const kLoginApiKey;